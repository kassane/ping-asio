# ping-asio

Ping CLI application cross-platform written in C++

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

- [asio]    - For network programming
- [cxxopts] - For command line option parsing
- [fmt]     - Formatting library
- [stx]     - Error-handling and utility extensions

### Installing

Clone this repository:

```bash
git clone https://gitlab.com/kassane/ping-asio.git
```

Navigate to the root and run cmake, the final executable (`ping`) will be in the `build` directory.

```bash
cmake -Bbuild -S. -DCMAKE_BUILD_TYPE=Release
cmake --build build -j
```

Run the program (note: (Linux) you may need to be root | (Windows) UAC privilege).

See `--help` for more options

```bash
ping google.com -t 40
172.217.8.206:0: seq=0, time=14.372ms:
1 packets transmitted, 0.000% loss
min/avg/max/ = 14.372/14.372/14.372 ms

172.217.8.206:0: seq=1, time=18.273ms:
2 packets transmitted, 0.000% loss
min/avg/max/ = 14.372/16.322/18.273 ms

...
```

## Acknowledgments

- **Chris Kohlhoff** - asio - [chriskohlhoff](https://github.com/chriskohlhoff)
- **jarro2783** - cxxopt - [jarro2783](https://github.com/jarro2783)
- **Basit Ayantunde** - STX - [lamarrr](https://github.com/lamarrr)
- **Victor Zverovich** - fmt - [vitaut](https://github.com/vitaut)

[asio]: https://github.com/chriskohlhoff/asio
[cxxopts]: https://github.com/jarro2783/cxxopts
[fmt]: https://github.com/fmtlib/fmt
[stx]: https://github.com/lamarrr/STX