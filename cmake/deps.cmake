#==================== DEPENDENCES =================================
include(FetchContent)
message(STATUS "-----------------------------------------")

message(STATUS "CXXOPTS  =>  Downloading")
FetchContent_Declare(
  opts
  GIT_REPOSITORY https://github.com/jarro2783/cxxopts.git
  GIT_TAG        master
)

message(STATUS "ASIO     =>  Downloading")
FetchContent_Declare(
  asio
  GIT_REPOSITORY "https://github.com/chriskohlhoff/asio.git"
  GIT_TAG        asio-1-18-0
)

message(STATUS "STX      =>  Downloading")
FetchContent_Declare(
  stx
  GIT_REPOSITORY "https://github.com/lamarrr/STX.git"
  GIT_TAG        v1.0.1
)

message(STATUS "FMT      =>  Downloading")
FetchContent_Declare(
    fmt
    GIT_REPOSITORY "https://github.com/fmtlib/fmt.git"
    GIT_TAG     7.1.0
)

FetchContent_GetProperties(asio)
if(NOT asio_POPULATED)
    FetchContent_Populate(asio)
endif()
set(ASIO_PATH ${asio_SOURCE_DIR})

FetchContent_GetProperties(opts)
if(NOT opts_POPULATED)
    FetchContent_Populate(opts)
endif()

set(CXXOPTS_PATH ${opts_SOURCE_DIR})
FetchContent_GetProperties(fmt)
if(NOT fmt_POPULATED)
    FetchContent_Populate(fmt)
endif()
set(FMT_PATH ${fmt_SOURCE_DIR})

message(STATUS "-----------------------------------------")
message(STATUS "ASIO    =>  Downloaded")
FetchContent_MakeAvailable(asio)
message(STATUS "CXXOPTS =>  Downloaded")
FetchContent_MakeAvailable(opts)
message(STATUS "FMT     =>  Downloaded")
FetchContent_MakeAvailable(fmt)
message(STATUS "STX     =>  Downloaded")
FetchContent_MakeAvailable(stx)
message(STATUS "-----------------------------------------")

add_subdirectory(${FMT_PATH} ${CMAKE_CURRENT_BINARY_DIR}/fmt EXCLUDE_FROM_ALL)
include_directories(${FMT_PATH}/include)
include_directories(${ASIO_PATH}/asio/include)
include_directories(${CXXOPTS_PATH}/include)
