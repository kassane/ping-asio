/*
    Copyright (c) 2019-2020 Contributors as noted in the AUTHORS file
    This file is part of ping-asio
    Distributed under the Boost Software License, Version 1.0. (See accompanying
    file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
*/

#pragma once

#include <iostream>

class Packet {
public:
  enum { reply = 0, request = 8 };

  Packet();
  Packet(uint16_t identifier, uint16_t sequence);
  uint16_t get_type() const;
  uint16_t get_identifier() const;
  uint16_t get_sequence() const;

  friend std::ostream &operator<<(std::ostream &os, const Packet &request);
  friend std::istream &operator>>(std::istream &is, Packet &response);

private:
  static const size_t data_len = 4;
  static const size_t data_byte_len = 8;
  static const size_t type_code_index = 0;
  static const size_t checksum_index = 1;
  static const size_t identifier_index = 2;
  static const size_t sequence_index = 3;
  static const uint16_t type_code = 0x800;

  uint16_t header[data_len]{};

  uint16_t compute_checksum();
};

std::ostream &operator<<(std::ostream &os, const Packet &request);
std::istream &operator>>(std::istream &is, Packet &response);
