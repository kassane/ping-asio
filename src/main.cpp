#include <cxxopts.hpp>
#include <fmt/color.h>
#include <fmt/core.h>
#include <pinger.hpp>
#include <span>
#include <stx/panic.h>

namespace opts = cxxopts;

void stx::panic_handler(
    std::string_view const &info, stx::ReportPayload const &payload,
    [[maybe_unused]] stx::SourceLocation const &source_location) noexcept {
  fmt::print("Error Occurred\nInfo: {}\nPayload: {}\n", info, payload.data());
}

void validate_timeout(const int milliseconds) {
  constexpr int max_timeout = 10000;
  constexpr int min_timeout = 10;
  if (milliseconds >= min_timeout && milliseconds <= max_timeout) {
    return;
  }
  fmt::print(fmt::fg(fmt::color::orange),
             "Timeout must be in range of [{}, {}] ms", min_timeout,
             max_timeout);
}

icmp::endpoint resolve_endpoint(asio::io_context &io_context,
                                const std::string &address,
                                const std::string &service = "") {
  const icmp::resolver::query query(icmp::v4(), address, service);
  icmp::resolver resolver(io_context);
  return *resolver.resolve(query);
}

opts::ParseResult parse_options(opts::Options &options, int &argc,
                                char **argv) {
  const std::string defualt_timeout = "5000";
  options.add_options()("h,help", "Print usage")(
      "t,timeout", "Number of milliseconds before a request times out",
      opts::value<int>()->default_value(defualt_timeout));
  options.custom_help("[OPTION...] destination");

  try {
    opts::ParseResult options_result = options.parse(argc, argv);

    if (options_result.count("help") != 0U) {
      fmt::print("{}\nYou may need to run this program as root\n",
                 options.help());
      exit(1);
    }

    if (options_result.count("timeout") != 0U) {
      validate_timeout(options_result["timeout"].as<int>());
    }

    return options_result;

  } catch (const std::exception &e) {
    stx::panic(fmt::format(fmt::fg(fmt::color::crimson), "{}", e.what()), &e);
  }
}

icmp::endpoint destination(asio::io_context &io_context, // int &argc,
                           std::span<char *> argv) {
  if (argv.size() == 1) {
    fmt::print("Missing ip address or domain name, see --help for more info\n");
    exit(1);
  }

  std::string arg;
  try {
    for (auto &value : argv) {
      arg = value;
    }
    return resolve_endpoint(io_context, arg);
  } catch (const std::exception &e) {
    stx::panic(
        fmt::format(fmt::fg(fmt::color::red), "Unable to resolve endpoint"),
        arg);
  }
}

int main(int argc, char *argv[]) {
  opts::Options options("Ping",
                        "Send/recive icmp echo packets to/from an endpoint");
  std::span arg(argv, argc);
  opts::ParseResult options_result = parse_options(options, argc, argv);

  asio::io_context io_context;

  icmp::endpoint endpoint = destination(io_context, arg);

  asio::chrono::milliseconds timeout_duration =
      asio::chrono::milliseconds(options_result["timeout"].as<int>());

  try {
    Pinger pinger(io_context, endpoint, timeout_duration);
    io_context.poll();
  } catch (const asio::system_error &se) {
    stx::panic(fmt::format(fmt::fg(fmt::color::crimson),
                           "{}\nYou may need to run this program as root",
                           se.what()),
               &se);
  }

  return 0;
}
